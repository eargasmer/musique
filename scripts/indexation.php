<?php
    require("modeles/_init.php");
    require("configs/config.php");
    require("configs/bdd.php");
    var_dump($arborescence);



    foreach($musique_path as $i => $Path)
    {
        
    	if(isset($argv[1]))
    	{
            if($argv[1] === $Path)
            {
                recursive_readdir($i,$Path,"/",$grep_path,$ffmpeg_path,$bdd,0,"none","none","none",$arborescence[$i]);
            }
            else
            {
                echo $Path." : pas indexé";
            }
    	}
	    else
	    {
            recursive_readdir($i,$Path,"/",$grep_path,$ffmpeg_path,$bdd,0,"none","none","none",$arborescence[$i]);
    	}
    }
    

    //$piste = new Piste_data();
    //$piste->chargerFlac("D:\\test\\B.B. King\\Back in the Alley\\01 Sweet Little Angel.flac",'C:\\Program Files\\Git\\usr\\bin\\grep','A:\\apps\\ffmpeg\\bin\\ffmpeg.exe','\\');
    
  
    function recursive_readdir ($biblio,$dir,$delimiter,$grep,$ffmpeg,$bdd,$niveau,$auteur,$album,$titre,$arbo) {
        
        $dir = rtrim ($dir, '/'); // on vire un eventuel slash mis par l'utilisateur de la fonction a droite du repertoire
        if (is_dir ($dir)) // si c'est un repertoire
            $dh = opendir ($dir); // on l'ouvre
        else 
        {
            echo $dir, ' n\'est pas un repertoire valide'; // sinon on sort! Appel de fonction non valide
            exit;
        }
        while (($file = readdir ($dh)) !== false ) 
        {
            if ($file !== '.' && $file !== '..') 
            { 
                $path =$dir.$delimiter.$file; // construction d'un joli chemin...
                if (is_dir ($path)) { //si on tombe sur un sous-repertoire 
                    echo "\n\n$path\n\n";
                    echo $niveau;
                    if(isset($arbo[$niveau]))
                    {
                        if($arbo[$niveau] == "auteur")
                        {
                            $auteur = $file;
                        }
                        else if($arbo[$niveau] == "album")
                        {
                            $album = $file;
                        }
                        else if($arbo[$niveau] == "piste")
                        {
                            $titre = $file;
                        }
                    }
                    recursive_readdir ($biblio,$path,$delimiter,$grep,$ffmpeg,$bdd,($niveau+1),$auteur,$album,$titre,$arbo); // appel recursif pour lire a l'interieur de ce sous-repertoire
                }
                else
                {
                    if(isset($arbo[$niveau]))
                    {
                    	echo "erreur architecture! - pas d'indexation";
                    }
                    else
                    {
                        $ext = pathinfo($file,PATHINFO_EXTENSION);
                                    echo "\n";
                        if($ext == "flac")
                    	{
                        	
				            $piste = new PisteManager($bdd);
                        	$res = $piste->chargerFlac($biblio,$path,$grep,$ffmpeg,$delimiter,$auteur,$album,$titre);
                        	echo "\nFLA ".$file." - ".$res->info()['album']." - ".$res->info()['auteur']." - ".$biblio;
                        	$piste->save($res);
                        	                        
                    	}
                    	else if($ext == "mp3")
                    	{
                        	$piste = new PisteManager($bdd);
                        	$res = $piste->chargerFlac($biblio,$path,$grep,$ffmpeg,$delimiter,$auteur,$album,$titre);
                        	echo "\nMP3 ".$file." - ".$res->info()['album']." - ".$res->info()['auteur']." - ".$biblio;
                        	$piste->save($res);
                    	}
		            }
                }
                    
            }
        }
        closedir ($dh); // on ferme le repertoire courant
    }	





    function indexation($fichier,$grep,$ffmpeg,$delimiter)
    {
        
        $meta = shell_exec ("$ffmpeg -i \"$fichier\" 2>&1");

        preg_match("/ARTISTS\ +:\ [0-9a-zA-Z\ ]+/",$meta,$res);                    
        $auteur = MetaGetValue($res);

        preg_match("/ALBUM\ +:\ [0-9a-zA-Z\ ]+/",$meta,$res);
        $album = MetaGetValue($res);

        preg_match("/TITLE\ +:\ [0-9a-zA-Z\ ]+/",$meta,$res);
        $titre = MetaGetValue($res);
        
        preg_match("/Duration: [0-9:]+/",$meta,$res);
        $duree = str_replace('Duration: ', '', $res[0]);
        
        echo $fichier."  ->  ".$titre."  ->  ".$auteur."  ->  ".$album."  ->  ".$duree."\n" ;
    }

    

?>
