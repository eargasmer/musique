<?php 
    $bdd->query('CREATE TABLE tags
                (
                    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
                    nom CHAR(100),
                    couleur char(100)
                )');

    $bdd->query('CREATE TABLE tagsToAlbum
                (
                    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
                    id_tag CHAR(100),
                    id_album char(100)
                )');
?>