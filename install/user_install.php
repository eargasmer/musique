<?php
    if(! empty($installation_valide))
    {
        if($installation_valide == 1)
        {
            $path = pathinfo(__FILE__)['dirname'];


            echo "\n\033[33m Start creating user \n \033[36m";
            if(! is_dir("$path/../configs/users"))
                mkdir("$path/../configs/users");
            echo "\n";
            $pseudo = readline('Admin username?');
            echo "\n";
            $mdp = readline('Admin password?');
            echo "\n";
            $mdp2 = readline('Confirm password?');
            $admin = new User_data();

            if ($admin->create('admin','admin',$pseudo,$mdp,$mdp2,2))
                $admin->setValid();

        }
        else
        {
            throw new Exception('installation token invalide');
        }
            
    }
    else
    {
        throw new Exception('installation refusé');
    }    
?>