<?php 
    echo "\033[32m - Create table auteur\n";
    $bdd->query('CREATE TABLE auteur
                (
                    id      INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
                    nom     char(100),
                    image   CHAR(255) DEFAULT \'none\',
                    biblio  CHAR(50)
                )');
?>