<?php
    $path = pathinfo(__FILE__)['dirname'];
    if(! empty($installation_valide))
    {
        if($installation_valide == 1)
        {
            //lance les scripts d'installtion des modèles
            echo "\n\033[33m Start installing models...\n\n";
            include($path."/drop.php");
            include($path."/auteur_install.php");
            include($path."/album_install.php");
            include($path."/piste_install.php");
            include($path."/tags_install.php");
            include($path."/user_install.php");
            
            echo "\n\n\033[33m To start to index music files:\033[37m scripts/indexation.php \n";
            #include($path."/indexation.php");
    
        }
        else
            throw new Exception('installation token invalide');
    }
    else
    {
         throw new Exception('installation refusé');
    }

    

?>