<?php 
    echo "\033[32m - Create table album\n";
    $bdd->query('CREATE TABLE album
                (
                    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
                    nom CHAR(100),
                    image CHAR(255),
                    id_auteur INT,
                    biblio  CHAR(50),
                    favori TINYINT DEFAULT 0
                )');
?>