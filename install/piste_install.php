<?php 
    echo "\033[32m - Create table piste\n";
    $bdd->query('CREATE TABLE piste
                (
                    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
                    nom CHAR(100),
                    fichier CHAR(255),
                    id_auteur INT,
                    id_album INT,
                    duree INT,
                    numero INT,
                    biblio CHAR(50)
                )');
?>