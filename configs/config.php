<?php
    $maintenance = false;
    $version = "2.1";
    $status = "beta";
    $name = "Musique";

    $control_path = 'controlleurs/';
    $vue_path = 'pages/';
    $modele_path = 'modeles/';
    $install_path = 'install/';

    $bdd_host = "localhost";
    $bdd_name = "musique";
    $bdd_user = "musique";
    $bdd_pass = "musique";

    $musique_path["biblio 1"] = "/mnt/divers/tempo";
    $musique_path["biblio 2"] = "/mnt/divers/tempo2";
    $GLOBALS['musique_path'] = $musique_path;

    //how music is stored ( values are auteur,album,piste )
    $arborescence["biblio 1"] = ['auteur','album'];// biblio 1 = <singer name>/<album title> track name will be found in metadata
    $arborescence["biblio 2"] = [];// biblio 2: all will be found in metadata

    //we use grep and ffmpeg to get metadata when indexing
    $grep_path = "/bin/grep";
    $ffmpeg_path = "/usr/bin/ffmpeg" ;

    //where php sessions are stored
    $session_path = $_SERVER['DOCUMENT_ROOT']."/sessions/";

    $meta = false;
    //this is url of the music converter server (metrome for version < 2.1 and sulli for version >= 2.1 ), replace localhost with ip or adress of your server 
    $metronome_url = 'http://localhost:3000'; 
    
    $theme = "darkorange";
    $GLOBALS['theme'] = $theme;
    $icone_theme = "/static/images/neon";
    $GLOBALS['icone_theme'] = $icone_theme;

    session_save_path($session_path); //specify the path to sessions
?>
