<?php
try 
{ 
    $bdd = new PDO("mysql:host=$bdd_host;dbname=$bdd_name", $bdd_user, $bdd_pass); 
    $GLOBALS['bdd'] = $bdd;
} 
catch (Exception $e) 
{        
    die('Erreur : ' . $e->getMessage()); 
} 
?>