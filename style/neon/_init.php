<?php    
    $path = pathinfo(__FILE__)['dirname'];
    $neon1 = "white";
    $neon2 = "hsla(0,0%,100%,.3)";

    include($path."/body.css");
    include($path."/contenu.css");
    include($path."/lecteur.css");
    include($path."/header.css");
    include($path."/album.css");
    include($path."/tags.css");
?>