<?php
    ob_start();
    header('content-type: text/css');
    $path = pathinfo(__FILE__)['dirname'];

    require($_SERVER['DOCUMENT_ROOT']."/configs/config.php");
    //liste des feuilles de styles à inclure
    include($path."/font.css");
    include($path."/template.css");
    include($path."/auteur.css");
    include($path."/album.css");
    include($path."/login.css");
    include($path."/index.css");
    include($path."/lecteur.css");
    include($path."/detail_album.css");
    include($path."/landing.css");
    include($path."/search.css");
?>
    @media only screen and (max-width: 1100px) {
<?php
    
    include($path."/tablette-template.css");
    include($path."/tablette-detail_album.css");
?>
    }

    @media only screen and (max-width: 750px) {
<?php
    include($path."/mobile-template.css");
    include($path."/mobile-detail_album.css");
?>
    }
<?php
    //minify css
    $buffer = ob_get_clean();
    $buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
    $buffer = str_replace(': ', ':', $buffer);
    $buffer = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $buffer);
    echo($buffer);
?>