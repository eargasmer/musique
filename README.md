Harmony - V 2.1
==============
Le but de ce projet est de mettre en place une interface web pour écouter et parcourir de la musique distante.

Harmony est dévellopé en php 7, html 5, css 3, et javascript vanillia.

Harmony est dépendant d'un service de convertion de musique: 
* Pour la version < 2.1 il faut [métronome](http://gitlab.etude.eisti.fr/eargasmer/metronome).
* Pour la version > 2.1 il faut [sulli](https://gitlab.etude.eisti.fr/darbouxtom/stratus)

 `La v2 n'est pas rétrocompatible avec la v1`
 
 `Pour passer de la v2 à la v2.1 il faut relancer l'installation et l'indexation`

 Une version 3 est à l'ordre du jour en python avec une api rest pour une maintainabilité plus simple et plein de nouvelles fonctionnalités du turfu, cette version ne devrait plus être devellopé, mais elle fonctionne pas trop mal alors enjoy !

Installation
------------
* éditez le fichier `configs/config.php` avec vos convenances
* lancez `php install.php`
* indexez vos albums avec `php scripts/indexation.php`
* c'est bon ! vous pouvez demmarer le serveur soit avec `php -S adresse` soit avec votre serveur web 

Nouveauté de la v2.1
--------------------
 * gestion des mp3 (gràce à Sulli)
 * interface revue
 * meilleur gestion des bibliothèques
 * gestion de l'historique du navigateur
 * ajout de la recherche
 * divers fixs


Nouveautés de la v2
------------------

La version 2 apporte son lot de nouveautés :
* indexation des fichiers locaux plutôt que de devoir uploader les albums
* interface revue et moderne
* programmation MVC pour une meilleure maintenance et évolution future.
* configuration plus simple
* installation automatique via un script php
* possibilité de changer de thème facilement
* navigation et lecteur amélioré
  * le lecteur est présent sur toutes les pages et on peut naviguer en écoutant sa musique

Todo important
--------------
* mettre un nom aux bibliothèques - fait!
* regler bug historique (retour biblio default) - fait!
* recherche global (auteur - piste - albums) dans plusieurs biblios
* indexation automatique si changement de fichiers
* minifier le css - fait!
* trouver un solution pour que le chargement de trop nombreux albums de fasse pas de ralentissement
* mettre en pause avec la barre espace
* fix le lecteur full-screen


Todo
----
* mettre en place des playlists
* permettre la modif des infos d'un album par un admin => ce sera peut-être l'objet d'un autre projet
* permettre d'indexer depuis le web
* mettre des tags
* develloper une page de compte utilisateur
* relier le projet à une api de connection => avec Sulli... ?
* trouver un moyen pour faire un miroir qui s'actualise sur github :p (faire un hook)
* mettre en place la possibilité de trier les albums

Previews
--------
![capture](static/readme/home.png)

![capture](static/readme/albums.jpg)


![capture](static/readme/mobile.png)
