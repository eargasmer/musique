<?php
// Turn off all error reporting
error_reporting(0);

global $_min;

require("configs/config.php");
require("configs/bdd.php");

function debug_to_console( $data ) {

    $STDOUT = fopen("php://stdout","w");
    if ( is_array( $data ) )
        $output = implode( ',', $data);
    else
        $output = $data;
    fwrite($STDOUT,"\n".$output."\n\n");
    fclose($STDOUT);
}

function getArgs()
{
	$args = parse_url($_SERVER['REQUEST_URI'])["query"] ?? null;
	parse_str($args, $output);
	return $output;
}

//on inclue les vues

require($vue_path.'_init.php');

// on inclue les controlleurs

require($control_path.'_init.php');

// on inclue les modeles
require($modele_path.'_init.php');

 session_start();

 if(!isset($_SESSION['user']))
     $_SESSION['user'] = new User_data();

// on définis les chemins
$urls = array(
    "index" => $acceuil,
    "" => $acceuil,
    "albums" => $albums,
    "lecteur" => $lecteur,
    "login" => $login,
    "about" => $about,
    "logout" => $logout,
    "album-detail" => $albumDetail,
    "favori" => $favori,
    "unfavori" => $unfavori,
    "convert" => $convert,
    "auteur" => $auteur,
    "popup_tag" => $popup_tag,
    "compte" => $compte
);

if(isset($_internalErreur) || $maintenance)
{
	$erreur500($_internalErreur);
	debug_to_console($_internalErreur);
}
else
{
	//on parse l'url et on execute le bon controlleur
	
		$url_page = $_SERVER['REQUEST_URI'];
		$path = explode('/',$_SERVER['REQUEST_URI']);
        //$GLOBALS["url"] = explode('?',$_SERVER['REQUEST_URI'])[0];

		if($path[1] == 'min')
		{
			array_splice($path,1,1);
			$_min = 1;
		}
		else
		{
			debug_to_console('lancement de la page');
		}
		$path[1] = explode('?',$path[1])[0];
        
		$control = $urls[$path[1]] ?? $erreur404;
		$control($path);
		//echo $path[1];
}


?>
