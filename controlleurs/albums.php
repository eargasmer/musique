<?php
    $albums =
        function($args)
        {
            if($_SESSION['user']->is_connected())
            {
                
				if($_SESSION['user']->is_ok())
				{
					$albumBdd = new AlbumManager($GLOBALS['bdd']);

					$id_auteur = getArgs()['auteur'] ?? NULL;
					
					if(isset(getArgs()['biblio']))
						$biblios = [getArgs()['biblio']];
					else
						$biblios = $_SESSION['user']->getBiblio();
					
					if($biblios[0] == "default" || empty($biblios[0]))
						$biblios = $_SESSION['user']->getBiblio();

					$albums = [];
					foreach($biblios as $biblio)
					{
						if(isset(getArgs()['s']))
							$albums = $albumBdd->listAlbum($id_auteur,$biblio,getArgs()['s']);
						else
							$albums = $albumBdd->listAlbum($id_auteur,$biblio);
					}
					$tags = [];
					$favoris = $albumBdd->getFavoris();
					$new_args = array(
							'albums' => $albums,
							'favoris' => $favoris,
							'tags' => $tags,
							);
					$vue = new vue("albums");
					$vue->aff($new_args);
					if(isset($args[2]))
					{
						echo "<script>console.log('detail...');detail(".$args[2].")</script>";
					}
				}
				else
				{
					$ok = false;
					$new_args = array(
						'message' => new infoMessage('Votre Compte n\'a pas été validé','erreur')
						);
					$vue = new vue("acceuil");
					$vue->aff($new_args);  
				}
			}
            else
            {
                $ok = false;
                $args = array(
                        'message' => new infoMessage('Accès refusé','erreur')
                    );
                $vue = new vue("login");
                $vue->aff($args);  
            }
            
             
            
             
        };
?>
