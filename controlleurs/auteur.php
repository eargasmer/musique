<?php
    $auteur =
        function($args)
        {
            if($_SESSION['user']->is_connected())
            {
                $auteurBdd = new AuteurManager($GLOBALS['bdd']);

                if(isset(getArgs()['biblio']))
                    $biblios = [getArgs()['biblio']];
                else
                    $biblios = $_SESSION['user']->getBiblio();
                
                if($biblios[0] == "default" || empty($biblios[0]))
                    $biblios = $_SESSION['user']->getBiblio();

                $albums = [];
                foreach($biblios as $biblio)
                    if(isset(getArgs()['s']))
                        $artistes = $auteurBdd->listAuteur($biblio,getArgs()['s']);
                    else
                        $artistes = $auteurBdd->listAuteur($biblio);

                $new_args = array(
                                'artistes' => $artistes
                            );
                $vue = new vue("auteurs"); 
                $vue->aff($new_args);
            }
            else
            {
                $ok = false;
                $args = array(
                        'message' => new infoMessage('Accès refusé','erreur')
                    );
                $vue = new vue("login");
                $vue->aff($args);
            }
        };
?>