<?php
    $path = pathinfo(__FILE__)['dirname'];

    //liste des controlleurs à inclure
    include($path."/404.php");
    include($path."/500.php");
    include($path."/acceuil.php");
    include($path."/albums.php");
    include($path."/lecteur.php");
    include($path."/login.php");
    include($path."/about.php");
    include($path."/logout.php");
    include($path."/album-detail.php");
    include($path."/favori.php");
    include($path."/convert.php");
    include($path."/auteur.php");
    include($path."/popup_tag.php");
    #include($path."/playlist.php");
    include($path."/compte.php");
    
    #include($path."/file.php");
?>
