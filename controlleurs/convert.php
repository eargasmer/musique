<?php
    $convert =
        function($args)
        {
            if ($_SERVER['REQUEST_METHOD'] === 'GET' && ! empty($_GET['link']))
            {
                $source = $_SERVER['DOCUMENT_ROOT']."/".$_GET['link'];
                
                
                if(verif_link($source))
                {
                    
                    $output = hash('sha512',$source).".mp3";
                    $output = "/static/cache/320/".$output;
                    //echo $source;
                    //echo $output;
                    if(! is_file($_SERVER['DOCUMENT_ROOT'].$output))
                    {
                        
                        $kbps = 320;
                        $mp3Stream = new Mp3Stream();
                        $mp3Stream->output($source, $_SERVER['DOCUMENT_ROOT'].$output, $kbps);
                    }
                    else
                    {

                        $output = $_SERVER['DOCUMENT_ROOT'].$output;
                        header('Cache-Control: public, must-revalidate, max-age=0');
                        header('Pragma: no-cache');
                        header('Accept-Ranges: bytes');
                        header('Content-length: ' . filesize($output));
                        header('Content-type: audio/mpeg');
                        header("Content-Disposition: inline; filename=\"{$output}\"");
                        header("Content-Transfer-Encoding: binary");
                        readfile($output);
                    }
                        
                    //echo $output;
                
                }
                else
                {
                    header('HTTP/1.0 404 Not Found');
                }
                    
            } 
            else
            {
                header('HTTP/1.0 404 Not Found');
            }
        };
    //($_SERVER['DOCUMENT_ROOT'])
    //verifie que le chemin de la musique est correct
    function verif_link($link)
    {
        
        if(!empty($link) && is_file($link))
            return true;
        else
            return false;
    }
?>