<?php
    //exemple de controlleur pour les erreur 404

    $albumDetail =
        function($args)
        {
            if(isset($args[2]))
            {
                
                $albumBdd = new AlbumManager($GLOBALS['bdd']);
                $album = $albumBdd->detailAlbum($args[2]);
                $args = array(
                                'album' => $album
                            );
                $vue = new vue("album-detail"); 
                $vue->aff($args);
            }
            else
            {
                $vue = new vue("404");
                $vue->aff([]);
            }
        };
?>