<?php
    //exemple de controlleur pour les erreur 404

    $lecteur =
        function($args)
        {
            if(isset($args[2]))
            {
                if($args[2] != 'full')
                {
                    $vue = new vue("lecteur");
                    $vue->minimal([]);
                }
                else
                {
                    $vue = new vue("lecteur-full");
                    $vue->aff([]);
                }
            }
            else
            {
                $vue = new vue("lecteur");
                $vue->minimal([]);
            }
        };
?>