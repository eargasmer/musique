<?php
    //lis un fichier de la bibliotheque de musique
    //setlocale(LC_ALL,'en_US.UTF-8');
    $STDOUT = fopen("php://stdout","w");
    include($_SERVER['DOCUMENT_ROOT']."/configs/config.php");
    //$musique_path = ["D:/biblio"];
    if(! empty($_GET['file']))
    {
		    
        $getfile = urldecode($_GET['file']);
        if($getfile == "default.png")
        {
            $file = $_SERVER['DOCUMENT_ROOT']."/static/images/default.png";
        }
        else
        {   
            foreach($musique_path as $biblio_id => $Path)
            {
                $file = $Path.$getfile;
                if(is_file($file))
                {
                    break;
                }
            }
        }        
        set_time_limit(8000);
        
        $url_file = $metronome_url.'/file?path='.urlencode($getfile)."&biblio=".$biblio_id;
        header('Location: '.$url_file);
    }
    else
    {
        header('HTTP/1.0 404 Not Found');
        $vue = new vue("404");
        $vue->aff([]);
    }
        
    

    function detectFileMimeType($filename='')
    {
        $finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
        $mime = finfo_file($finfo, $filename);
        finfo_close($finfo);
        return $mime;
    }
    
    fclose($STDOUT);
?>
