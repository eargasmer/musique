<?php

    $login =
        function($args)
        {
            $args = [];
            if($_SESSION['user']->is_connected())
            {
                    $vue = new vue("acceuil");
            }
            else if(!(class_exists('DOMDocument')))
            {
            
                $ok = false;
                $args = array(
                        'message' => new infoMessage('Erreur interne : DOMDocument non installé','erreur')
                    );
                $vue = new vue("login");            
            }
            else
            {
                if ($_SERVER['REQUEST_METHOD'] === 'POST') 
                {
        
                    if(!empty($_POST['pseudo']) && !empty($_POST['mdp']) && !empty($_POST['mdp2']) && !empty($_POST['nom']) && !empty($_POST['prenom']) )
                    {
                        $ok = $_SESSION['user']->create($_POST['nom'],$_POST['prenom'],$_POST['pseudo'],$_POST['mdp'],$_POST['mdp2']);

                        if($ok)
                        {
                            $args = array(
                                        'message' => new infoMessage('Compte créé','succes')
                                    );
                            $vue = new vue("acceuil");  
                        }
                        else
                        {
                            $args = array(
                                        'message' => new infoMessage('L\'inscription a échoué','erreur')
                                    );
                            $vue = new vue("login");       
                        }
                    }
                    else if (!empty($_POST['pseudo']) && !empty($_POST['mdp']))
                    {
                        
                        $ok = $_SESSION['user']->login($_POST['pseudo'],$_POST['mdp']);
                                    
                        if($ok)
                        {
                            
                            $args = array(
                                        'message' => new infoMessage('OK','succes')
                                    );
                            $vue = new vue("acceuil");  
                        }
                        else
                        {
                            
                            $args = array(
                                        'message' => new infoMessage('Pseudo ou mot de passe incorrect','erreur')
                                    );
                            $vue = new vue("login");       
                        }
                        
                    }
                    else
                    {   
                        $ok = false;
                        $args = array(
                                    'message' => new infoMessage('Veuillez remplir tous les champs','erreur')
                                );
                        $vue = new vue("login");            
                    }
                }
                else
                {
                $vue = new vue("login");
                }
            }
            $vue->aff($args);
        };
?>
