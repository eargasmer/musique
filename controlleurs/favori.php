<?php
    $favori =
        function($args)
        {
            if(! empty($args[2]))
            {
                $albumBdd = new AlbumManager($GLOBALS['bdd']);
                $valid = $albumBdd->favori($args[2]);
                
                header('HTTP/1.0 204 No Content');
            }
            else
            {
                header('HTTP/1.0 404 Not Found');
                $vue = new vue("404");
                $vue->aff([]);
            }
        };
    
    $unfavori = 
        function($args)
        {
            if(! empty($args[2]))
            {
                $albumBdd = new AlbumManager($GLOBALS['bdd']);
                $valid = $albumBdd->unfavori($args[2]);
                
                header('HTTP/1.0 204 No Content');
            }
            else
            {
                header('HTTP/1.0 404 Not Found');
                $vue = new vue("404");
                $vue->aff([]);
            }
        }
?>