function Piste(nom,auteur,duree,pochette,lien,hash,album)
{
    console.log("-> piste : "+ auteur + " - "+nom + " - " + duree + " - " + " - " + album); 
    this.auteur = auteur;
    this.nom = nom;
    this.duree = duree;
    this.pochette = pochette;
    this.lien = lien;
    this.hash = hash;
    this.album = album;
}

/*
 - ajouter une piste a la liste
 - supprimer une piste
 - remmettre le lecteur à 0
 - pause/play / precedent / suivant
 - creer le lecteur
    - si il existe dans le stockage local -> charge avec le local
    - sinon remet à 0

*/


function Lecteur2()
{
    this.pile_lecture = [];
    this.nombre = 0;
    this.enCours = 0;
    this.lecteur = null;
    this.audioTrack = null;

    /********************************************************
     *       Fonction ajout / supression pistes             *
     * ******************************************************/
    
    this.addPiste = function(piste)
    {
        var source = document.getElementById('source_lecteur');

        this.pile_lecture.push(piste);
        sessionStorage.setItem("pile_lecture",JSON.stringify(this.pile_lecture));  
        
        if (this.audioTrack.ended || source.src == '')
        {
            this.play()
        }
    }

    //this.deletePiste();
    
    /********************************************************
     *      Fonction suivant/precedent... etc               *
     * ******************************************************/

    this.player = function() 
    {
        var playButton = document.getElementById("play_butt");
        
        if (this.audioTrack.paused) 
        {
            playButton.src = icone_theme+"/pause.svg";
            this.audioTrack.play();            
        } 
        else 
        {
            this.pause();
        }
    }

    this.play = function()
    {
        var playButton = document.getElementById("play_butt");        
        var source = document.getElementById('source_lecteur');
        var piste = this.pile_lecture[this.enCours];  
        var sheet = document.createElement('style');

        document.title = piste.nom+" - Alchain";
        playButton.src = icone_theme+"/pause.svg";        
        source.src = piste.lien;

        this.setPiste(piste.nom);
        this.setPochette(piste.pochette);

        this.audioTrack.load();
        this.audioTrack.play();

        this.removeStyle()

        
        sheet.id='style_piste';
        sheet.innerHTML = "."+piste.hash+"{background-color: <?= $GLOBALS['theme'] ?>;}";
        document.body.appendChild(sheet);

        sessionStorage.setItem("enCours",this.enCours);
        
        
    }

    this.pause = function()
    {
        var playButton = document.getElementById("play_butt");
        playButton.src = icone_theme+"/play.svg";
        this.audioTrack.pause();
    }

    this.suiv = function()
    {
        this.enCours = parseInt(this.enCours) +1;
        if(this.enCours < this.pile_lecture.length)
            this.play();
        else
            this.enCours = this.pile_lecture.length-1;
        
        sessionStorage.setItem("enCours",this.enCours);
    }

    this.prec = function()
    {
        this.enCours = parseInt(this.enCours) -1;
        if(this.enCours < 0)
        {
            this.enCours = 0;
        }
        this.play();
    }

    this.playn = function(i)
    {
        this.enCours = i;
        if(this.enCours < 0)
        {
            this.enCours = 0;
        }
        this.play();
    }
    
   this.finish = function()
    {
        this.suiv();
    }

    /********************************************************************
    **              Fonction de création du lecteur                    **
    ********************************************************************/
    this.maximise = function()
    {
        console.log('max!');
        try {
            var detail = document.getElementById('detail_album_conteneur');
            detail.parentNode.removeChild(detail);
        }
        catch(err)
        {

        }

        var xhr = getXHR();
	var this2 = this;
        xhr.onreadystatechange = function()
        {
            if (xhr.readyState == 4)
            {

                if(xhr.status == 200)
                {
                    reponse = xhr.responseText;
                    document.getElementById('content').innerHTML += reponse;
                    var maximise_content = document.getElementById('detail_album');
                    var pistes = maximise_content.getElementsByClassName('pistes')[0];
                    var titre = maximise_content.getElementsByClassName('titre_album')[0];
                    var auteur = maximise_content.getElementsByClassName('auteur')[0];
                    auteur.innerHTML = this2.pile_lecture[this2.enCours].auteur;
                    titre.innerHTML = this2.pile_lecture[this2.enCours].album;
                    this2.setPochette(this2.pile_lecture[this2.enCours].pochette);
                    for(var i=0;i<this2.pile_lecture.length;i++)
                    {
                        ligne = "<li class='"+this2.pile_lecture[i].hash+"' onclick='lecteur.playn("+i+")'>"
                        ligne += "<span class='numero'>"+i+"</span>- ";
                        ligne += "<span class='nom_piste'>"+this2.pile_lecture[i].nom+"</span>";
                        ligne += "<span class='duree'>"+this2.pile_lecture[i].duree+"</span>";            
                        ligne += "</li>";
                        pistes.innerHTML += ligne;
                    }
                    
                }
            }
        }

        // cas de la méthode post
        xhr.open("GET","/min/lecteur/full/",false);
        xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded;charset=utf-8');
        xhr.send();
    }

    this.removeStyle = function()
    {
        var sheetToBeRemoved = document.getElementById('style_piste');
        if(sheetToBeRemoved != null)
        {
            var sheetParent = sheetToBeRemoved.parentNode;
            sheetParent.removeChild(sheetToBeRemoved);
        }
        
    }

    this.setPiste = function(nom)
    {
        var lecteur = document.getElementById('player');
        //var info = lecteur.getElementsByClassName('center')[0];
        lecteur.getElementsByClassName("piste")[0].innerHTML = nom;
    }

    this.setPochette = function(pochette)
    {
        var lecteur = document.getElementById('player');
        var couv = lecteur.getElementsByClassName('couverture')[0];
        var couv2 = document.getElementById('img-lecteur-full');
        couv.style.backgroundImage = "url('/"+pochette+"')";
        if(couv2 != null)
            couv2.src = "/"+pochette+"";
    }

    this.free = function()
    {
        this.enCours =  0;
        this.pile_lecture = [];; 
        this.nombre = 0;
        this.setPiste('(pas de pistes)');
    }

    this.set = function()
    {
        this.enCours = parseInt(sessionStorage.getItem("enCours")) || 0;
        this.pile_convertion = [];
        this.pile_lecture = JSON.parse(sessionStorage.getItem("pile_lecture")) || [];;
        this.setPiste('(pas de pistes)');

        this.audioTrack = document.getElementById("audiotrack");        
        
        //evenement quand la piste est fini
        this.audioTrack.addEventListener('ended', this.finish.bind(this), false);
        //evenement bouton play/pause
        document.getElementById("play_butt").addEventListener('click',this.player.bind(this),false);
        document.getElementById("suiv_butt").addEventListener('click',this.suiv.bind(this),false);
        document.getElementById("prec_butt").addEventListener('click',this.prec.bind(this),false);
        document.getElementById("max_butt").addEventListener('click',this.maximise.bind(this),false);
        //evenement quand l'user change le temps sur le slide
        
        if(this.pile_lecture.length > 0)
	{
		this.play();
	}
    }

    this.init = function()
    {
        xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() 
        {
            if (xhr.readyState == 4)
            {
                
                if(xhr.status == 200)
                {
                    ancienLecteur = document.getElementById('player');
                    if(ancienLecteur == null)
                    {
                        document.getElementById("nav-left").innerHTML += xhr.responseText;
                        
                    }
                      
                }
            }
        }  
        // cas de la méthode post
        xhr.open("GET","/lecteur",false); 
        xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded;charset=utf-8');
        xhr.send();
    }

    
}
