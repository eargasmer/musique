window.onload = function(){
	page = location.pathname.split('/')[1];
	console.log('lien-'+page)
	document.getElementById('lien-'+page).id = 'menu-select';
}


function getJsonFromUrl() 
{
	var query = location.search.substr(1);
	var result = {};
	if(query.trim())
	{
		query.split("&").forEach(
			function(part) 
			{
				var item = part.split("=");
				result[item[0]] = decodeURIComponent(item[1]);
			}
		);
	}
	return result;
}

function dictToUrl(dict)
{	
	if(dict)
	{
		var params = ""
		var keys = Object.keys(dict);
		var i = 0;
		keys.forEach(
			function(key)
			{
				if(i > 0)
					params+="&"+key+"="+encodeURIComponent(dict[key])
				else
					params+="?"+key+"="+encodeURIComponent(dict[key])
				i++
			}
		)
		return params
	}
	else
		return ""
}


function Navigation()
{
	this.url = window.location.pathname;
	this.params =  getJsonFromUrl();

	this.link = function(url,params,setHistory = true)
	{
		
		var finalUrl = url+dictToUrl(params);
		
		this.params = params;
		this.url = url;
		
		var xhr = getXHR();
		xhr.onreadystatechange = function()
		{
			if (xhr.readyState == 4)
			{
	
				if(xhr.status == 200)
				{
					reponse = xhr.responseText;
					document.getElementById('content').innerHTML = reponse;	
					if(setHistory)				
						history.pushState({}, "", finalUrl);
				}
				else
				{
					alert("oups une erreur est survenu lors de l'ouverture du lien");
				}
			}
		}
		// cas de la méthode post
		xhr.open("GET","/min"+finalUrl,false);
		xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded;charset=utf-8');
		xhr.send();
	}

	this.addParam = function(key,value)
	{
		this.params[key] = value
	}

	this.deleteParam = function(key)
	{
		delete(this.params[key])
	}

	this.setParams = function(params)
	{
		this.params = params
	}

	this.go = function(setHistory = true)
	{
		this.link(this.url,this.params,setHistory)
	}

	this.setUrl = function(url)
	{
		this.url = url;
	}

}
/*


//remet tous les liens dans leurs états originels
function clear_link()
{
    try{

        document.getElementById('menu-select').id = '';

    }catch(e){
    }
     
}
*/

window.addEventListener('popstate', function(event) {
	//alert(window.location.pathname + window.location.search)
	navigation.setParams(getJsonFromUrl)
	navigation.setUrl(window.location.pathname)
	navigation.go(false)

}, true);

function getXHR()
{
	var xhr = null;
	if (window.XMLHttpRequest) // FF & autres
		xhr = new XMLHttpRequest();
	else if (window.ActiveXObject) { // IE < 7
		alert("Votre navigateur ne supporte pas AJAX");
	}
	return xhr;
}
