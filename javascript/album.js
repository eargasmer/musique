/*function selectPiste(piste)
{
	lecteur.set();
	piste.play();
}
*/
var tag_popup = null;



function aff_control_piste(ele)
{
	parent = ele.parentNode;
	control  = parent.getElementsByClassName('control')[0];
	if(control.style.display == "block")
		control.style.display = "none";
	else
		control.style.display = "block";

}

function album_unfavori(ele,id)
{
	var xhr = getXHR();
	
	
	
	xhr.onreadystatechange = function()
	{
		if (xhr.readyState == 4)
		{

			if(xhr.status == 204)
			{
				link(null,'albums','albums');	
				ele.className='favori';
			}
		}
	}

	xhr.open("GET","/unfavori/"+id,true);
	xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded;charset=utf-8');
	xhr.send();	
}

//met en favori un album (id = id de l'album)
function album_favori(ele,id)
{
	var xhr = getXHR();
	
	
	
	xhr.onreadystatechange = function()
	{
		if (xhr.readyState == 4)
		{

			if(xhr.status == 204)
			{
				link(null,'albums','albums');	
				ele.className+=' active';
			}
		}
	}

	xhr.open("GET","/favori/"+id,true);
	xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded;charset=utf-8');
	xhr.send();	
}


function hideDetail()
{	
	var detail = document.getElementById('detail_album_conteneur');
	detail.parentNode.removeChild(detail);
	console.log("hide details");
	//history.pushState({ foo: "bar" }, "Album", "/albums");
}


//charge les detail d'un album et l'affiche au premier plan
function detail(id)
{
    try {
        var detail = document.getElementById('detail_album_conteneur');
        detail.parentNode.removeChild(detail);
    }
    catch(err)
    {

    }

    var xhr = getXHR();
	xhr.onreadystatechange = function()
	{
		if (xhr.readyState == 4)
		{

			if(xhr.status == 200)
			{
				history.pushState({ foo: "bar" }, "Album - detail", "/albums/"+id);
				reponse = xhr.responseText;
                document.getElementById('content').innerHTML += reponse;
			}
		}
	}

	// cas de la méthode post
	xhr.open("GET","/min/album-detail/"+id,true);
	xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded;charset=utf-8');
	xhr.send();
}


function getXHR()
{
	var xhr = null;
	if (window.XMLHttpRequest) // FF & autres
		xhr = new XMLHttpRequest();
	else if (window.ActiveXObject) { // IE < 7
		alert("Votre navigateur ne supporte pas AJAX");
	}
	return xhr;
}

function clic_biblio(biblio,ele)
{
	navigation.deleteParam("biblio")
	navigation.deleteParam("s")
	navigation.addParam("biblio",biblio)
	navigation.go()

	var links = Array.from(ele.parentElement.getElementsByTagName('li'));
	links.forEach(
		function(part) 
		{
			if(part.classList.contains("selected"))
				part.classList.remove("selected");
		}
	);

	ele.classList.add("selected")
}

function clic_auteur(id_auteur)
{
	navigation.deleteParam("auteur")
	navigation.deleteParam("s")
	navigation.addParam("auteur",id_auteur)
	navigation.setUrl("/albums")
	navigation.go()
}
