class LeftMenu {
    constructor() 
    {
        this.visible = true;
    }

    button()
    {
        var menu = document.getElementById("nav-left");
        if (this.visible) 
        {
            
            menu.style.display = "none";

        }
        else 
        {
            menu.style.display = "flex";            
        }
        this.visible = !this.visible
    }
}




function mobileMenu()
{
	    if(document.getElementById('menu-navigation').style.display!="block")
            {
                 document.getElementById('menu-navigation').style.display="block";
            }
            else
            {
                document.getElementById('menu-navigation').style.display="none";
            }	
}