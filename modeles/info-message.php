<?php

class infoMessage
{
    private $value;
    private $type;

    public function __construct($val,$type)
    {
        $this->value = $val;
        $this->type = $type;
    }

    public function aff()
    {
        echo "<div class='msg-conteneur'><div class='msg-".$this->type."'>".$this->value."</div></div>";
    }
}
?>