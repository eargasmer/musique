<?php


class Album 
{
    private $auteurs;
    private $nom;
    private $image;
    private $id;
    private $favori;
    private $biblio;

    public function __construct($nom,$image,$auteurs,$id,$favori,$biblio)
    {
            $this->nom = $nom;
            $this->image = $image;
            $this->auteurs = $auteurs;  
            $this->id = $id; 
            $this->favori = $favori;
            $this->biblio = $biblio;         
    }

    public function getAuteur()
    {
        return $this->auteurs;
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getImg()
    {
        if(isset($this->image))
            return "controlleurs/file.php?file=".urlencode(str_replace($GLOBALS['musique_path'][$this->biblio],'',$this->image));
        else
            return 'static/images/default.png';
    }

    public function getFavori()
    {
        return $this->favori;
    }
}


class AlbumManager
{
    private $bdd;

    public function __construct($bdd)
    {
        $this->bdd = $bdd;
    }

    //ajoute un album dans la bdd (si il y est pas déja)
    public function add($nom,$id_auteur,$biblio)
    {
        $req = $this->bdd->prepare('SELECT COUNT(*) AS nombre FROM album WHERE nom = :nom');
        $req->execute( array( 
                                'nom' => $nom
                            ));
        $data = $req->fetch();

        if( $data['nombre'] == 0 )
        {
            $req = $this->bdd->prepare('INSERT INTO album(nom,id_auteur,biblio) VALUES (:nom,:id,:biblio)');
            $req->execute( array( 
                                'nom' => $nom,
                                'id' => $id_auteur,
                                'biblio' => $biblio
                            ));
            return $this->bdd->lastInsertId();        
        } 
        else
        {
            $req = $this->bdd->prepare('SELECT id FROM album WHERE nom = :nom ');
            $req->execute( array( 'nom' => $nom ));
            
            if($data = $req->fetch())
                return $data['id'];
            else
                return 0;
        }
    }

    //recupère la liste des auteurs d'un album
    private function auteurs($id_album)
    {
        $req =  $this->bdd->prepare("SELECT * FROM (SELECT id_auteur FROM piste WHERE id_album = :id) as piste INNER JOIN auteur ON piste.id_auteur = auteur.id;");
        $req->execute( array( 'id' => $id_album ));

        $auteurs = array();
        while($data = $req->fetch())
        {
            $auteurs[] = $data['nom'];
        }
        
        //debug_to_console('auteur - '.$id_album.' ->'.($auteurs[0] ?? 'inconnu'));
        return $auteurs;

    }

    //renvoi une liste avec tous les albums de la bdd
    public function listAlbum($auteur="",$biblio="",$search="")
    {   
        $albums = array();
        if(! empty($auteur))
        {
            $req =  $this->bdd->prepare("SELECT * FROM album WHERE id_auteur = :auteur");
            $req->execute( array( 
                'auteur' => $auteur
            ));
        }
        else
            if(isset($biblio))
            {
                $req = $this->bdd->prepare('SELECT * FROM album WHERE biblio = :biblio AND nom like :search');
                $req->execute( array( 
                    'biblio' => $biblio,
                    'search' => "%$search%"
                ));
            }
            else
                $req =  $this->bdd->query("SELECT * FROM album");
        
        while($data = $req->fetch())
        {
            $albums[] = new album($data['nom'],$data['image'],$this->auteurs($data['id']),$data['id'],$data['favori'],$data['biblio']);
            
        }
        return $albums;
    }

    public function get($id)
    {
        $req =  $this->bdd->query("select * from album WHERE id=".$id);
        $data = $req->fetch();
        $album = new album($data['nom'],$data['image'],$this->auteurs($data['id']),$data['id'],$data['favori'],$data['biblio']);
        return $album;
    }
    public function detailAlbum($id)
    {
        $req =  $this->bdd->query("select * from album WHERE id=".$id);
        $data = $req->fetch();
        $album = new album($data['nom'],$data['image'],$this->auteurs($data['id']),$data['id'],$data['favori'],$data['biblio']);
        $img = $data['image'];
        $req =  $this->bdd->prepare("SELECT * FROM piste WHERE id_album = :id ORDER BY numero ASC");
        $req->execute( array( 'id' => $id ));

        

        $pistes = array();
        while($data = $req->fetch())
        {
            //debug_to_console($data['id_auteur']);
            $pistes[] = new piste($data['biblio'],$data['nom'],$data['id_auteur'],$data['nom'],$data['duree'],$data['fichier'],$data['numero'],$img);
        }

        $res = array(
                        "album" => $album,
                        "pistes" => $pistes
                    );

        return $res;

    }

    public function setImage($img,$id)
    {
        $req =  $this->bdd->prepare("UPDATE album SET image = :img WHERE id = :id");
        $req->execute( array( 'id' => $id, 'img' => $img ));
        echo "\najout img: $img";
    }

    public function getFavoris()
    {
        $albums = array();

        $req =  $this->bdd->query("SELECT * from album WHERE favori = 1");
        while($data = $req->fetch())
        {
            $albums[] = new album($data['nom'],$data['image'],$this->auteurs($data['id']),$data['id'],$data['favori'],$data['biblio']);
            
        }
        return $albums;
    }

    public function favori($id)
    {
        $req =  $this->bdd->prepare("UPDATE album SET favori = 1 WHERE id = :id");
        $req->execute( array( 'id' => $id));
    }

    public function unfavori($id)
    {
        $req =  $this->bdd->prepare("UPDATE album SET favori = 0 WHERE id = :id");
        $req->execute( array( 'id' => $id));
    }
}

?>
