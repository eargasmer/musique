<?php

class User_data
{
    private $nom;
    private $prenom;
    private $mdp;
    private $pseudo;
    private $rang;
    private $connected;
    private $ok;
    public function __construct()
    {
        $this->logout();
    }

    public function is_ok()
    {
	    return ($this->ok === "1");
    }

    public function logout()
    {
        $this->connected = false;
    }

    public function is_connected()
    {
        return $this->connected;
    }

    public function is_admin()
    {
        return ($this->rang == 2);
    }

    public function Pseudo()
    {
        return $this->pseudo;
    }

    public function create($nom,$prenom,$pseudo,$mdp,$mdp2,$rang=0)
    {
        if($mdp == $mdp2)
        {
            if (!file_exists("configs/users/".strtolower($pseudo).".xml"))//on verifie que le pseudo est pas déja utilisé
            {
                $fichier=fopen("configs/users/".strtolower($pseudo).".xml","w+");//dans le cas contraire créé le compte
                fputs($fichier,"<user>\n");
                fputs($fichier,"<nom>".htmlspecialchars($nom)."</nom>\n");
                fputs($fichier,"<prenom>".htmlspecialchars($prenom)."</prenom>\n");
                fputs($fichier,"<mdp>". hash('tiger192,3', htmlspecialchars($mdp))."</mdp>\n");
                
                fputs($fichier,"<admin>".$rang."</admin>\n");

                $biblios = "<biblios>\n";

                foreach ($GLOBALS['musique_path'] as $key => $value)
                {
                    $biblios = $biblios."   <biblio>".$value."</biblio>\n";
                }

                $biblios = $biblios."</biblios>\n";


                fputs($fichier,$biblios);
                fputs($fichier,"<theme></theme>\n");
                fputs($fichier,"<ok>0</ok>\n");
                fputs($fichier,"</user>");
                fclose($fichier);

                $this->pseudo = $pseudo;
                $this->nom = $nom;
                $this->prenom = $prenom;


                return true;
            }
            else
                return false;
        }
        else
            return false;
    }

    public function levelUp()
    {   
        $this->rang = 2;

        $doc = new DOMDocument();
        $doc -> load("configs/users/".$this->pseudo.".xml");
        $nom = $doc -> getElementsByTagName('admin') -> item(0) ;
        $nom -> nodeValue = $this->rang;
        $doc -> save("configs/users/".$this->pseudo.".xml");
        
    }

    public function setValid()
    {
        $doc = new DOMDocument();
        $doc -> load("configs/users/".$this->pseudo.".xml");
        $nom = $doc -> getElementsByTagName('ok') -> item(0) ;
        $nom -> nodeValue = 1;
        $doc -> save("configs/users/".$this->pseudo.".xml");
    }

    public function login($login,$pwd)
    {
	    if (file_exists("configs/users/".strtolower($login).".xml") && (class_exists('DOMDocument')))
        {
            $doc = new DOMDocument();
            $file = "configs/users/".strtolower($login).".xml";
            $doc->load($file);
            //on recupère le mot de passe
            $mdp = $doc->getElementsByTagName("mdp")->item(0)->nodeValue;
            $ok = $doc->getElementsByTagName("ok")->item(0)->nodeValue;
            //on verifie que ca correspond
            if($mdp == hash('tiger192,3', $pwd))
            {
                $this->connected = true;
                $this->nom = $doc->getElementsByTagName("nom")->item(0)->nodeValue;
                $this->prenom = $doc->getElementsByTagName("prenom")->item(0)->nodeValue;
                $this->pseudo = $login;
                $this->mdp = $mdp;
		$this->ok = $ok;
            }
        }

        return $this->connected;
    }

    public function ajoutBiblio($name)
    {
        $doc = new DOMDocument();
        $doc -> load("configs/users/".$this->pseudo.".xml");
        $biblios = $doc -> getElementsByTagName('biblios') -> item(0) ;
        $biblio -> createElement("biblio");
        $biblio->nodeValue($name);
        $biblios->appendChild($biblio);
        $doc -> save("configs/users/".$this->pseudo.".xml");
    }

    public function getBiblio()
    {
        $doc = new DOMDocument();
        $doc -> load("configs/users/".$this->pseudo.".xml");
        $biblios = $doc -> getElementsByTagName('biblio');
        $res = array();
        foreach($biblios as $i=>$biblio)
        {
            array_push($res,$biblio->nodeValue);
        }
        return $res;
    }

    public function supprBiblio()
    {
        echo "toto";
    }

    public function ajoutCleTheme($key,$value)
    {
        echo "toto";
    }

    public function supprCleTheme($key)
    {
        echo "toto";
    }
}

?>
