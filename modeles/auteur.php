<?php
class Auteur
{
    private $nom;
    private $image;
    private $albums;
    private $id;
    private $biblio;

    public function __construct($nom,$image,$id,$biblio)
    {
            $this->nom = $nom;
            $this->image = $image;
            $this->id = $id;
            $this->biblio = $biblio;        
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getBiblio()
    {
        return $this->biblio;
    }

    public function getImg()
    {
        return 'static/images/auteur3.png';
    }
}

class AuteurManager
{
    private $bdd;

    public function listAuteur($biblio,$search="")
    {   
        $albums = array();

        
        $req = $this->bdd->prepare('SELECT * from auteur WHERE biblio = :biblio AND nom LIKE :search ORDER BY nom ASC');
        $req->execute( array( 
                                'biblio' => $biblio,
                                'search' => "%$search%"
                            ));
        while($data = $req->fetch())
        {
            $albums[] = new Auteur($data['nom'],'',$data['id'],$biblio);
            
        }
        return $albums;
    }

    public function __construct($bdd)
    {
        $this->bdd = $bdd;
    }

    public function add($nom,$biblio)
    {
        $req = $this->bdd->prepare('SELECT COUNT(*) AS nombre FROM auteur WHERE nom = :nom AND biblio = :biblio');
        $req->execute( array( 
                                'nom'   => $nom,
                                'biblio'=> $biblio
                            ));
        $data = $req->fetch();

        if( $data['nombre'] == 0 )
        {
            $req = $this->bdd->prepare('INSERT INTO auteur(nom,biblio) VALUES (:nom,:biblio)');
            $req->execute( array( 
                                'nom'    => $nom,
                                'biblio' => $biblio
                            ));
            return $this->bdd->lastInsertId();        
        } 
        else
        {
            $req = $this->bdd->prepare('SELECT id FROM auteur WHERE nom = :nom AND biblio = :biblio ');
            $req->execute( array( 
                'nom'   => $nom,
                'biblio'=> $biblio
            ));
            
            if($data = $req->fetch())
                return $data['id'];
            else
                return 0;
        }
    }

    public function list()
    {

    }
}


?>