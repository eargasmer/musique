<?php



class Streamer
{

    public function outputStream($cmd, $byteGoal,$output)
    {   
        $descriptorspec = array(
            0 => array("pipe", "r"),  // stdin is a pipe that the child will read from
            1 => array("pipe", "w"),  // stdout is a pipe that the child will write to
            2 => array("file", "./error-output.txt", "a") // stderr is a file to write to
        );

        $cwd = './';
        $env = NULL;
        
        $process = proc_open($cmd, $descriptorspec, $pipes, $cwd, $env);
        $tot = "";
        if (is_resource($process)) {
            // $pipes now looks like this:
            // 0 => writeable handle connected to child stdin
            // 1 => readable handle connected to child stdout
            // Any error output will be appended to /tmp/error-output.txt
            fclose($pipes[0]);
            while (!feof($pipes[1])) {
                    $content = fread($pipes[1],1);
                    $tot .= $content;
                    echo $content;
            }
            
            fclose($pipes[1]);

            $monfichier = fopen($output, 'a'); 
            fputs($monfichier,$tot);
            fclose($monfichier);
        }
    }

    protected function getPadding($outputSize, $byteGoal)
    {
        // If we still haven't reached our goal, fill the remaining bytes with
        // "0"
        if($outputSize <= $byteGoal){
            return str_pad('', $byteGoal - $outputSize, '0');
        }

        return '';
    }
}
