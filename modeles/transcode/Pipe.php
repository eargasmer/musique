<?php

class Pipe
{

    private $handle;

    public function open($cmd, $mode)
    {
        $this->handle = popen($cmd, $mode);
    }

    public function fread($bytes)
    {
        $toto = fread($this->handle, $bytes);
        return $toto;
    }

    public function feof()
    {
        return feof($this->handle);
    }

    public function close()
    {
        return pclose($this->handle);
    }
}
