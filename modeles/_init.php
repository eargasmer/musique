<?php
    $path = pathinfo(__FILE__)['dirname'];
    //liste des modèles à inclure
    include($path."/user.php");
    include($path."/album.php");
    include($path."/auteur.php");
    include($path."/piste.php");
    include($path."/info-message.php");
    include($path."/tags.php");
    //... le decodeur mp3 ...
    #require($path."/transcode/AudioInspector.php");
    #require($path."/transcode/HeaderBuilder.php");
    #require($path."/transcode/Streamer.php");
    #require($path."/transcode/TimeCalculator.php");
    #require($path."/transcode/TranscodedSizeEstimator.php");
    #require($path."/transcode/Transcoder.php");
    #require($path."/transcode/Mp3Stream.php");
?>
