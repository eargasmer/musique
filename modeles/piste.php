<?php


class Piste
{
    private $nom;
    private $fichier;
    private $auteur;
    private $album;
    private $duree;
    private $numero;
    private $id;
    private $hash;
    private $image;
    private $biblio;

    public function __construct($biblio,$nom,$auteur,$album,$duree,$fichier,$numero,$image='static/images/default.png')
    {
        $this->nom = $nom;
        $this->auteur = $auteur;
        $this->album = $album;  
        $this->duree = $duree;
        $this->fichier = $fichier; 
        $this->numero = $numero;
        $this->image = $image;
        $this->biblio = $biblio;
        $this->hash = 'piste_'.md5($this->nom).md5($this->auteur);

    }

    public function setId($id)
    {
        if(!isset($this->id))
            $this->id = $id;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    private function getImage()
    {
        //$this->debug_to_console($this->biblio.' '.$this->image);
        if(isset($this->image))
            return "controlleurs/file.php?file=".urlencode(str_replace($GLOBALS['musique_path'][$this->biblio],'',$this->image));
        else
            return 'static/images/default.png';
    }
    public function getDuree()
    {
        if(!empty($this->duree))
            $duree = $this->duree;
        else
            $duree = '??:??:??';
    }

    public function getFichier()
    {
        return "/controlleurs/file.php?file=".urlencode(str_replace($GLOBALS['musique_path'][$this->biblio],'',$this->fichier));
    }
    public function info()
    {
        
        $res = array (
                        'nom' => $this->nom,
                        'auteur' => $this->auteur,
                        'album' => $this->album,
                        'duree' => $this->duree,
                        'fichier' => $this->fichier,
                        'numero' => $this->numero,
                        'hash' => $this->hash,
                        'biblio' => $this->biblio
                    );
        return $res;
    }

    public function debug_to_console( $data ) {
        
            $STDOUT = fopen("php://stdout","w");
            if ( is_array( $data ) )
                $output = implode( ',', $data);
            else
                $output = $data;
            fwrite($STDOUT,"\ndebug... :\n\n".$output);
            fclose($STDOUT);
    }
    

    //retourne le code javascript pour lancer la piste dans le lecteur
    public function play()
    {        
	    echo "var piste = new Piste('".addslashes($this->nom)."','".addslashes($this->auteur)."','".addslashes($this->getDuree())."','".addslashes($this->getImage())."','".addslashes($this->getFichier())."','".$this->hash."','".addslashes($this->album)."');";
        echo 'lecteur.addPiste(piste);';
    }

}

class PisteManager
{
    private $bdd;
    
    public function __construct($bdd)
    {
        $this->bdd = $bdd;
    }
    //importe un fichier Flac et lit les metatdonnées
    public function chargerFlac($biblio,$fichier,$grep,$ffmpeg,$delimiter,$auteur="none",$album="none",$titre="none")
    {
                $cmd = "$ffmpeg -i \"$fichier\" 2>&1"; 
                $meta = shell_exec ($cmd);
                
                /*** AUTEUR ***/
                if($auteur == "none")
                {
                    preg_match("/ARTIST\ +:\ .+/",$meta,$res);                    
                    $auteur = $this->MetaGetValue($res);
                }
                if($auteur == "none")
                {
                    preg_match("/TS02\ +:\ .+/",$meta,$res);                    
                    $auteur = $this->MetaGetValue($res);
                }
                if($auteur == "none")
                {
                    preg_match("/album_artist\ +:\ .+/",$meta,$res);                    
                    $auteur = $this->MetaGetValue($res);
                }
                if($auteur == "none")
                {
                    preg_match("/artist\ +:\ .+/",$meta,$res);                    
                    $auteur = $this->MetaGetValue($res);
                }
                
                /*** ALBUM ***/
                if($album == "none")
                {
                    preg_match("/ALBUM\ +:\ .+/",$meta,$res);
                    $album = $this->MetaGetValue($res);
                }
                if($album == "none")
                {
                    preg_match("/album\ +:\ .+/",$meta,$res);
                    $album = $this->MetaGetValue($res);
                }
               
                /*** TITRE ***/
                if($titre == "none")
                {
                    preg_match("/TITLE\ +:\ .+/",$meta,$res);
                    $titre = $this->MetaGetValue($res);
                }
                if($titre == "none")
                {
                    preg_match("/title\ +:\ .+/",$meta,$res);
                    $titre = $this->MetaGetValue($res);
                }

                preg_match("/track\ +:\ .+/",$meta,$res);
                $numero = $this->MetaGetValue($res);
               
                preg_match("/Duration: [0-9:]+/",$meta,$res);
                $duree = trim(str_replace('Duration: ', '', $res[0]));

                //$fichier = addslashes($fichier);
                //$fichier = str_replace("é","%C3%A9",$fichier);
                $piste = new Piste($biblio,$titre,$auteur,$album,$duree,$fichier,$numero);
                return $piste;                    
       
    }

    //enregistre en bdd un morceau
    public function save($piste)
    {
        
        $valeur_Piste = $piste->info();

        //on ajoute l'auteur à la bdd et on récupère son id
        $auteur = new AuteurManager($this->bdd);
        $auteur_id = $auteur -> add($valeur_Piste['auteur'], $valeur_Piste['biblio']);

        //on ajoute l'album à la bdd et on récupère son id
        $albumManager = new AlbumManager($this->bdd);
        $album_id = $albumManager -> add($valeur_Piste['album'],$auteur_id,$valeur_Piste['biblio']);

        if(is_file(dirname($valeur_Piste['fichier'])."/Folder.jpg"))
        {
                $albumManager -> setImage(dirname($valeur_Piste['fichier'])."/Folder.jpg",$album_id);
                #$piste -> setImage(dirname($valeur_Piste['fichier'])."/Folder.jpg");
        }
        //on compte le nombre de piste pareil dans la bdd
        $req = $this->bdd->prepare('SELECT COUNT(*) AS nombre FROM piste WHERE nom = :nom AND id_auteur = :auteur AND id_album = :album ');
        $req->execute( array( 
                                'nom' => $valeur_Piste['nom'], 
                                'auteur' =>$auteur_id, 
                                'album' => $album_id
                            ));        

        $data = $req->fetch();

        //si la piste existe pas on l'ajoute
        if( $data['nombre'] == 0)//si c'est un nouveau morceau
        {
            echo "\n --- piste save... ---";
	    $req = $this->bdd->prepare('INSERT INTO piste(nom,id_auteur,id_album,fichier,numero,biblio) VALUES (:nom,:id_auteur,:id_album,:fichier,:numero,:biblio)');
            $req->execute( array( 
                                'nom' => $valeur_Piste['nom'], 
                                'id_auteur' =>$auteur_id, 
                                'id_album' => $album_id,
                                'fichier' => $valeur_Piste['fichier'],
                                'numero' => $valeur_Piste['numero'],
                                'biblio' => $valeur_Piste['biblio']
                            )); 
            $res = $req;
            $piste -> setId($this->bdd->lastInsertId());
        }
        else
        {
           $res = false;
        }

    }

    private function MetaGetValue($res)
    {
        
        if(isset($res[0]))
        {
            $str = $res[0];
            $str = explode(":",$str)[1];
            $str = explode(";",$str)[0];
            $str = str_replace("'"," ",$str);
            
        }
        else
        {
            $str = "none";
        }

        return trim($str);
    }
    
}

?>
