<?php
    class Tag
    {
        private $nom;
        private $couleur;
        private $id;

        public function __construct($nom,$couleur,$id)
        {
            $this->nom = $nom;
            $this->couleur = $couleur;
            $this->id = $id;
        }

        public function getNom()
        {
            return $this->nom;
        }

        public function getCouleur()
        {
            return $this->couleur;
        }
    }

    class TagsManager
    {
        private $bdd;

        public function __construct($bdd)
        {
            $this->bdd = $bdd;
        }

        public function add($nom,$couleur)
        {
            $req = $this->bdd->prepare('SELECT COUNT(*) AS nombre FROM tags WHERE nom = :nom');
            $req->execute( array( 
                                    'nom' => $nom
                                ));
            $data = $req->fetch();

            if( $data['nombre'] == 0 )
            {
                $req = $this->bdd->prepare('INSERT INTO tags(nom,couleur) VALUES (:nom,:couleur)');
                $req->execute( array( 
                                    'nom' => $nom,
                                    'couleur' => $couleur
                                ));
                return $this->bdd->lastInsertId();        
            } 
            else
            {
                $req = $this->bdd->prepare('SELECT id FROM auteur WHERE nom = :nom ');
                $req->execute( array( 'nom' => $nom ));
                
                if($data = $req->fetch())
                    return $data['id'];
                else
                    return 0;
            }
        }

        public function relier($tag,$album)
        {
            $req = $this->bdd->prepare('INSERT INTO tags(nom,album) VALUES (:tag,:album)');
            $req->execute( array( 
                                'id_tag' => $tag,
                                'id_album' => $album
                            ));
            return $this->bdd->lastInsertId();
        }
        public function listTags()
        {   
            /*$tags = array();

            $req =  $this->bdd->query("select * from tags");
            while($data = $req->fetch())
            {
                $tags[] = new Tag($data['nom'],$data['couleur'],$data['id']);
                
            }*/
            return [];
        }

    }
?>
