<?php
    $this->titre = 'Musique';
?>

<div class="albums">
    <h1>Les albums</h1>
    <div class="center">
<form class="search" action="albums">
    <input type="search" name="s" placeholder="chercher un album"/><input type="image" src="/static/helium/search.svg" alt="Submit Form" />
    <input type="hidden" name="biblio" value="<?=getArgs()['biblio']?>">
</form>
</div>
<?php
	if(isset($albums))
	{
		foreach ($albums as $album)
        {
             aff_album($album);
        }
	}
?>
</div>

<?php
    function aff_album($album)
    {
        ?>
            
            <div class="album" onclick="detail('<?= $album->getId();?>')">
                <div class='pochette' style='background-image:url("/<?=$album->getImg()?>");'>
                    
                </div>
                <div class='info'>
                <?= $album->getNom();?>



                </div>
            </div>
            

        <?php
    }
?>
