<?php
    $this->titre = 'Musique';
?>

<h1>Les artistes</h1>
<div class="center">
    <form class="search" action="auteur">
        <input type="search" name="s" placeholder="chercher un artiste"/><input type="image" src="/static/helium/search.svg" alt="Submit Form" />
        <input type="hidden" name="biblio" value="<?=getArgs()['biblio']?>">
    </form>
</div>
<?php
	if(isset($artistes))
	{
        
		foreach ($artistes as $artiste)
        {
                $lettre = strtoupper($artiste->getNom()[0]);
                if(! isset($ancienLettre))
                {
                    echo "<h2 class='lettre'>$lettre </h2>";
                    $ancienLettre = $lettre;
                }
                else if($lettre != $ancienLettre)
                {
                    echo "<h2 class='lettre'>$lettre </h2>";
                    $ancienLettre = $lettre;
                }
                ?>
                <div class="artiste" onclick='clic_auteur(<?php echo $artiste->getId() ?>)'>
                    <div class='image' style='background-image:url("/<?=$artiste->getImg()?>"); background-size:cover;'>
                    
                    </div>
                    <div class='info'>
                        <?= $artiste->getNom();?>
                    </div>
                </div>
                <?php
        }
	}
?>
