<?php
class Vue {

  private $page;
  private $titre;
  private $path;
  
  //constructeur de l'objet
  public function __construct($nom) 
  {
    $this->path = pathinfo(__FILE__)['dirname'];
    $this->page = $this->path."/".$nom.".php";
  }

  //affiche la vue sans le gabarit
  public function minimal($donnees)
  {
      $contenu = $this->genererFichier
      (
          $this->page, //url de la page
          $donnees //tableau associatif
      );

      echo $contenu;
  }

  //affiche la vue, enrobé dans le gabarit
  public function aff($donnees) 
  {
    if($GLOBALS['_min'] == '1')
    {
      $this->minimal($donnees);
    }
    else
    {
      // Génération de la partie spécifique de la vue
      
      $contenu = $this->genererFichier
      (
          $this->page, 
          $donnees
      );
      

      // Génération du gabarit commun utilisant la partie spécifique
      $vue = $this->genererFichier
      (
          $this->path.'/gabarit.php',
          array_merge(
            $donnees, 
            array
            (
                'titre' => $this->titre, 
                'contenu' => $contenu
            )
          )
      );

      echo $vue;
    }
  }

  // Génère un fichier vue et renvoie le résultat produit
  private function genererFichier($page, $donnees) 
  {
    if (file_exists($page)) 
    {
      // Rend les éléments du tableau $donnees accessibles dans la vue
      extract($donnees);
      // Démarrage de la temporisation de sortie
      ob_start();
      // Inclut le fichier vue
      // Son résultat est placé dans le tampon de sortie
      require $page;
      // Arrêt de la temporisation et renvoi du tampon de sortie
      return ob_get_clean();
    }
    else {
      throw new Exception("Fichier '$page' introuvable");
    }
  }
}
?>
