<!doctype html>
<html lang="fr">
    <head>
        <meta charset="UTF-8" />
        <link rel="stylesheet" href="/style/_init.php" />
        <script><?php include('javascript/_init.php'); ?></script>
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="shortcut icon" href="/static/images/favicon.png" type="image/png"/>
        <meta property="og:image" content="/static/images/favicon.png"/>
        <title>Musique - <?= $titre ?></title>
    </head>
    <body>

        <nav id="nav-top" >
            <a class="left invert">
                <img src="/static/helium/menu.svg" onclick="leftMenu.button()"/>
            </a>
                
                
            <a class="left invert">
                <img id='lien-user' src="/static/images/user.svg" />
            </a> 
                <?php if($_SESSION['user']->is_connected()){ ?>
            <a href="/logout" class="left invert">
                <img class='logout' src="/static/images/logout.svg"/>
            </a>
                
                <?php } ?>
            <!--<img class='icone phone' src="/static/images/menu.png" onclick="leftMenu.button()"/>-->
            <a class="menu-ele right" id="lien-auteur">
                <span class="no-phone">Artistes</span>
                <img src="/static/images/artiste.svg" class="phone"/>
            </a>
            <a class="menu-ele right" id="lien-albums">
                <span class="no-phone">Albums</span>
                <img src="/static/images/album2.svg" class="phone icone"/>
            </a>

        </nav>        
        <!-- content -->

        <div id="bottom" class="flex_grow">
            <nav id="nav-left" class="no-tablet">
            <!--
            -->
                <div class="flex_grow">
                    <div>
                        <h2>Bibliothèques</h2>
                        <ul>
                            <li class="selected" onclick="clic_biblio('default',this)">Default</li>
                            <?php foreach($GLOBALS['musique_path'] as $key => $biblio) { ?>

                            <li onclick="clic_biblio('<?= $key ?>',this)"><?=$key?></li>
                            
                            <?php } ?>
                        </ul>
                    </div>

                    <div>
                        <h2>Tri</h2>
                        <ul>
                                <li class="selected">Ordre Alphabétique</li>
                        </ul>
                    </div>
                </div>
                
            
            </nav>
            
            
            <section id="content"  class="flex_grow">
                <?= $contenu."\n" ?>
            </section>
        </div>
        <!-- start music reader -->        
        <script>
            navigation = new Navigation();
            leftMenu = new  LeftMenu();

            var lecteur = new Lecteur2();
            lecteur.init();
            lecteur.set();
            //var piste = new Piste('When We Were Young','1','','static/images/default.png','/Adele/25/01 Hello.flac','piste_cef3bf52eca7774d38b041993d60b76cc4ca4238a0b923820dcc509a6f75849b','hello');
            //lecteur.addPiste(piste);
        </script>
    </body>
</html>
