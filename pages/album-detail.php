<?php
    
?>

<?php
	if(isset($album))
	{   
        $this->titre = 'detail - '.$album['album']->getNom();
        ?>
        <div id='detail_album_conteneur'>
        
        <div id='detail_album'>
            <nav id='bar-detail'>
                <a onclick='hideDetail()'>
                    <img src='<?= $GLOBALS['icone_theme'] ?>/fleche.svg' />
                    Retour
                </a>
            </nav>
            <div class="pochette_conteneur">
                <div style='background-image:url("/<?= $album['album']->getImg() ?>")' class='pochette'></div>
            </div>
            <div class="flex-grow">
                <h3><?= $album['album']->getNom(); ?></h3>
                <p class='auteur'><?= $album['album']->getAuteur()[0] ?></p>
                <ul class='pistes'>
                <?php
                foreach($album['pistes'] as $key => $piste)
                {
                    $data = $piste->info();
                    ?>
                    <li class="<?= $data['hash'] ?>" onclick="(
                                    function()
                                    { 
                                        lecteur.free();
                                        <?php 
                                        
                                            for($i=$key;$i<count($album['pistes']);$i++)
                                            {
                                            ?>
                                                <?php
                                                    $album['pistes'][$i]->play();
                                                ?>
                                                ;
                                            <?php
                                            }
                                            
                                        ?>;
                                        lecteur.play(); 
                                    }
                                )()">
                        <span class='numero'><?= $data['numero'] ?> </span>-
                        <span class='nom_piste'><?= $data['nom'] ?></span>
                        <span class='duree'><?= $data['duree'] ?></span>
                    </li>
                <?php
                }
                ?>
                </ul>
            </div>


          
        </div>
        </div>
<?php
    }
?>
