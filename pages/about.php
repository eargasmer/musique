<?php
    $this->titre = 'A propos';
?>
<h1> A Propos </h1>
<h2>Qui? Comment?</h2>
Site web réalisé par Tom DARBOUX en php 7, javascript, html 5 et css 3,sans frameworks externe, testé sous ios9, blackberry .., firefox 50 pour pc ou android,opera 24 sous android 6, vivaldi 1.8.
<br/>
Avec l'aide de Gaël Berthaud-Müller,Nicolas Dejonghe et Elio maisonneuve.
<br/>
Open source, disponible sur <a href="http://gitlab.etude.eisti.fr/darbouxtom/musique">le gitlab de l'EISTI</a>
<h2> Sites webs qui m'ont le plus aidé</h2>
<ul>
    <li><a href='http://bpesquet.developpez.com/tutoriels/php/evoluer-architecture-mvc'>Un tutoriel sur le mvc en php</a></li>
    <li><a href="http://alsacreations.com"> Alsa création, un super site de trucs et astuces en css et html</a></li>
    <li><a href="http://stackoverflow.com/">Stack overflow, pour les nombreuses questions que je me suis posé</a></li>
    <li><a href="http://php.net/">La documentation php</a></li> 
    <li><a href="http://www.w3schools.com/">w3schools pour la documentation html et css</a></li>
    <li><a href="http://www.qwant.com/">Le moteur de recherche Qwant qui m'a permis de trouver les réponses adéquates</a></li>
    <li><a href="https://github.com/gadgetguru/PHP-Streaming-Audio">Mon convertisseur audio en php est fortement inspiré du projet 'PHP-Streaming-Audio' de gadgetguru</a></li>
</ul>
<h2>Les licenses</h2>
<div>Font made from <a href="http://www.onlinewebfonts.com">oNline Web Fonts</a> is licensed by CC BY 3.0</div>
<div>Certaines icones ont été déssinées par Madebyoliver,Freepik, EpicCoders  sur www.flaticon.com</div>
<div>La convertion audio utilise ffmpeg</div>